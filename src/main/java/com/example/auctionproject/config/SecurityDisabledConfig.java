package com.example.auctionproject.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@Profile(value = "without_security")
public class SecurityDisabledConfig {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeHttpRequests(auth -> {
            auth.requestMatchers("/api/user/create").permitAll();
            auth.requestMatchers("/api/auction/create").permitAll();
            auth.requestMatchers("/api/userAddress/create").permitAll();
            auth.requestMatchers("/api/bidding/create").permitAll();
            auth.requestMatchers("/api/bidding/getAllBidding").permitAll();
            auth.requestMatchers("/api/purchase/create").permitAll();
            auth.requestMatchers("/api/user/show").permitAll();
            auth.requestMatchers("/api/user/findByEmail/{email}").permitAll();
            auth.requestMatchers("/api/user/update").permitAll();


        }).httpBasic();

        httpSecurity.csrf().disable().authorizeHttpRequests()
                .and().cors().disable().authorizeHttpRequests();

        return httpSecurity.build();
    }
}

package com.example.auctionproject.config;

import com.example.auctionproject.model.UserRole;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@Profile(value = "with_security")
public class SecurityConfig {
    public static final String ROLE_USER = "user";
    public static final String ROLE_ADMIN = "admin";

    @Bean
    @Primary
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService(){
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(User.withUsername("user").password(passwordEncoder()
                .encode("12345")).roles(ROLE_USER).build());

        manager.createUser(User.withUsername("admin").password(passwordEncoder()
                .encode("12345")).roles(ROLE_ADMIN).build());

        manager.createUser(User.withUsername("power_user").password(passwordEncoder()
                .encode("12345")).roles(ROLE_ADMIN, ROLE_USER).build());

        return manager;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {

        return httpSecurity
                .csrf(csrf -> csrf.disable())
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers("/api/user/create").hasRole(ROLE_USER)
                        .requestMatchers("/api/auction/create").hasRole(ROLE_ADMIN)
                        .requestMatchers("/api/userAddress/create").hasRole(ROLE_USER)
                        .requestMatchers("/api/bidding/create").hasRole((ROLE_USER))
                        .requestMatchers("/api/bidding/getAllBidding").hasRole(ROLE_USER)
                        .requestMatchers("/api/purchase/create").hasRole(ROLE_ADMIN)
                        .requestMatchers("/api/user/show").hasRole(ROLE_ADMIN)
                        .requestMatchers("/api/user/findByEmail/{email}").hasRole(ROLE_ADMIN)
                        .requestMatchers("/api/user/update").hasRole(ROLE_USER)
                        .anyRequest().authenticated()
                )

                .httpBasic(Customizer.withDefaults())
                .build();
    }


}

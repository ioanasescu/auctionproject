package com.example.auctionproject.controller;

import com.example.auctionproject.dto.bidding.BiddingCreateDto;
import com.example.auctionproject.dto.bidding.BiddingDisplayDto;
import com.example.auctionproject.dto.purchase.PurchaseCreateDto;
import com.example.auctionproject.dto.purchase.PurchaseDisplayDto;
import com.example.auctionproject.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/purchase")
public class PurchaseController {
    private final PurchaseService purchaseService;

    @Autowired
    public PurchaseController(PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }
    @PostMapping("/create")
    public ResponseEntity<PurchaseDisplayDto> createPurchase(@RequestBody PurchaseCreateDto purchaseCreateDto){
        PurchaseDisplayDto purchaseDisplayDto = purchaseService.createPurchase(purchaseCreateDto);

        return ResponseEntity.ok(purchaseDisplayDto);
    }
}

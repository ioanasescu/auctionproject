package com.example.auctionproject.controller;

import com.example.auctionproject.dto.bidding.BiddingCreateDto;
import com.example.auctionproject.dto.bidding.BiddingDisplayDto;
import com.example.auctionproject.service.BiddingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/bidding")
public class BiddingController {
    private final BiddingService biddingService;

    @Autowired
    public BiddingController(BiddingService biddingService) {
        this.biddingService = biddingService;
    }

    @PostMapping("/create")
    public ResponseEntity<BiddingDisplayDto> createBidding(@RequestBody BiddingCreateDto biddingCreateDto){
        BiddingDisplayDto biddingDisplayDto = biddingService.createBidding(biddingCreateDto);

        return ResponseEntity.ok(biddingDisplayDto);
    }

    @GetMapping("/getAllBidding")
    public ResponseEntity<List<BiddingDisplayDto>> displayBidding(){
        return ResponseEntity.ok(biddingService.findAllBidding());
    }
}

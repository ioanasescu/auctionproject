package com.example.auctionproject.controller;

import com.example.auctionproject.dto.auction.AuctionCreateDto;
import com.example.auctionproject.dto.auction.AuctionDisplayDto;
import com.example.auctionproject.service.AuctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auction")
public class AuctionController {
    private final AuctionService auctionService;


    @Autowired
    public AuctionController(AuctionService auctionService) {
        this.auctionService = auctionService;
    }

    @PostMapping("/create")
    public ResponseEntity<AuctionDisplayDto> createUser(@RequestBody AuctionCreateDto auctionCreateDto){
        AuctionDisplayDto auctionDisplayDto = auctionService.createAuction(auctionCreateDto);

        return ResponseEntity.ok(auctionDisplayDto);
    }
}

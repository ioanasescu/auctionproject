package com.example.auctionproject.controller;

import com.example.auctionproject.dto.user.UserAddressCreateDto;
import com.example.auctionproject.dto.user.UserAddressDisplayDto;
import com.example.auctionproject.service.UserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/userAddress")
public class UserAddressController {
    private final UserAddressService userAddressService;

    @Autowired
    public UserAddressController(UserAddressService userAddressService) {
        this.userAddressService = userAddressService;
    }

    @PostMapping("/create")
    public ResponseEntity<UserAddressDisplayDto> createUserAddress(@RequestBody UserAddressCreateDto userAddressCreateDto){
        UserAddressDisplayDto userAddressDisplayDto = userAddressService.createUserAddress(userAddressCreateDto);
        return ResponseEntity.ok(userAddressDisplayDto);
    }
}

package com.example.auctionproject.controller;

import com.example.auctionproject.dto.user.UserCreateDto;
import com.example.auctionproject.dto.user.UserDisplayDto;
import com.example.auctionproject.dto.user.UserUpdateDto;
import com.example.auctionproject.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping("/create")
    public ResponseEntity<UserDisplayDto> createUser(@RequestBody UserCreateDto userCreateDto){
        UserDisplayDto userDisplayDto = userService.createUser(userCreateDto);
        return ResponseEntity.ok(userDisplayDto);
    }

    @GetMapping("/show")
    public ResponseEntity<List<UserDisplayDto>> findAllUsers() {

        return ResponseEntity.ok(userService.findAllUsers());
    }

    @GetMapping("/findByEmail/{email}")
    public ResponseEntity<UserDisplayDto> findUserByEmail(@PathVariable String email){
        return ResponseEntity.ok(userService.findByEmail(email));
    }

    @PutMapping("/update")
    public ResponseEntity<UserDisplayDto> updateUser(@RequestBody @Valid UserUpdateDto userUpdateDto){
        UserDisplayDto userDisplayDto = userService.updateUser(userUpdateDto);

        return ResponseEntity.ok(userDisplayDto);

    }

}

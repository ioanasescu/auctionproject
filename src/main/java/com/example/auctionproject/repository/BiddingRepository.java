package com.example.auctionproject.repository;

import com.example.auctionproject.model.Bidding;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BiddingRepository extends CrudRepository<Bidding, Long> {
    @Query(value = "SELECT max(amount) from Bidding")
    Integer findMaxAmount();
}


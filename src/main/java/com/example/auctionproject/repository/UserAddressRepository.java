package com.example.auctionproject.repository;

import com.example.auctionproject.model.User;
import com.example.auctionproject.model.UserAddress;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserAddressRepository extends CrudRepository<UserAddress, Long> {

}

package com.example.auctionproject.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String email;
    private String password;
    private String accountName;
    @OneToOne(cascade = CascadeType.ALL)
    private UserAddress userAddress;
    private LocalDateTime dateOfAccountCreation;
    @Column
    @Enumerated(EnumType.STRING)
    private UserAccountStatus accountStatus;
    private String avatar;
    @Column
    @Enumerated(value = EnumType.STRING)
    private UserType userType;
    @Column
    @Enumerated(value = EnumType.STRING)
    private UserRole userRole;
    @OneToMany(mappedBy = "user")
    private List<Auction> auctions;
    @OneToMany (mappedBy = "user")
    private List<Bidding> bidding;
    @OneToMany
    private List<Purchase> purchases;

}

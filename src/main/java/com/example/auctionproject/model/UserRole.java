package com.example.auctionproject.model;

public enum UserRole {
    USER,
    ADMIN;
}

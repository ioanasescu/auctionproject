package com.example.auctionproject.model;

import jakarta.persistence.*;
import lombok.*;
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Bidding {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn (name = "auctionBidding_id")
    private Auction auction;
    @ManyToOne
    @JoinColumn(name = "userBidding_id")
    private User user;
    private Integer amount;
    @OneToOne(cascade = CascadeType.ALL)
    private Purchase purchase;
}

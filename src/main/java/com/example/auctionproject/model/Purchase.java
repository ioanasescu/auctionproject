package com.example.auctionproject.model;

import jakarta.persistence.*;
import lombok.*;
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    private Auction auction;
    @ManyToOne
    private User user;
    @OneToOne(cascade = CascadeType.ALL)
    private Bidding bidding;
    private Integer amount;
}

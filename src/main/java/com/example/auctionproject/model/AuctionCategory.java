package com.example.auctionproject.model;

public enum AuctionCategory {
    APPLIANCES,
    PHONES,
    COMPUTERS,
    SPORT,
    COSMETICS,
    CLOTHES,
    ARTS,
    CARS;

}

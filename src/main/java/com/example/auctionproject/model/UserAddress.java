package com.example.auctionproject.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String province;
    private String city;
    private String street;
    private String houseNumber;
    private String zipCode;
    @OneToOne
    private Auction auction;
}

package com.example.auctionproject.model;

public enum UserAccountStatus {
    ACTIVE,
    INACTIVE,
    LOCKED;
}

package com.example.auctionproject.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Auction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private String photos;
    @Column
    @Enumerated(EnumType.STRING)
    private AuctionCategory auctionCategory;
    private Double minAmount;
    private boolean promoted;
    @OneToOne
    private UserAddress userAddress;
    private LocalDateTime dateOfIssue;
    private LocalDateTime endDate;
    private Integer numberOfVisits;
    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;
    @OneToMany (mappedBy = "auction")
    private List<Bidding> bidding;
    @OneToOne(cascade = CascadeType.ALL)
    private Purchase purchase;

    public void setAddress(){
        this.userAddress = user.getUserAddress();
    }

    public boolean isPromoted(){
       return user.getUserType() == UserType.PREMIUM ? promoted : !promoted;
    }
}

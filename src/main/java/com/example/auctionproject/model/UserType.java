package com.example.auctionproject.model;

public enum UserType {
    NORMAL,
    PREMIUM;
}

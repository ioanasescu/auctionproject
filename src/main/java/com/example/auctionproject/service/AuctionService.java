package com.example.auctionproject.service;

import com.example.auctionproject.dto.auction.AuctionCreateDto;
import com.example.auctionproject.dto.auction.AuctionDisplayDto;


public interface AuctionService {

    AuctionDisplayDto createAuction(AuctionCreateDto auctionCreateDto);
}

package com.example.auctionproject.service;

import com.example.auctionproject.dto.user.UserCreateDto;
import com.example.auctionproject.dto.user.UserDisplayDto;
import com.example.auctionproject.dto.user.UserUpdateDto;

import java.util.List;

public interface UserService {
    UserDisplayDto createUser(UserCreateDto userCreateDto);
    UserDisplayDto findByEmail(String email);
    List<UserDisplayDto> findAllUsers();
    UserDisplayDto updateUser(UserUpdateDto userUpdateDto);



}

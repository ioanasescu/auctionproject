package com.example.auctionproject.service;

import com.example.auctionproject.dto.purchase.PurchaseCreateDto;
import com.example.auctionproject.dto.purchase.PurchaseDisplayDto;

public interface PurchaseService {
    PurchaseDisplayDto createPurchase(PurchaseCreateDto purchaseCreateDto);
}

package com.example.auctionproject.service;

import com.example.auctionproject.dto.bidding.BiddingCreateDto;
import com.example.auctionproject.dto.bidding.BiddingDisplayDto;

import java.util.List;

public interface BiddingService {
    BiddingDisplayDto createBidding(BiddingCreateDto biddingCreateDto);
    List<BiddingDisplayDto> findAllBidding();
}

package com.example.auctionproject.service;

import com.example.auctionproject.dto.user.UserAddressCreateDto;
import com.example.auctionproject.dto.user.UserAddressDisplayDto;

public interface UserAddressService {
    UserAddressDisplayDto createUserAddress(UserAddressCreateDto userAddressCreateDto);
}

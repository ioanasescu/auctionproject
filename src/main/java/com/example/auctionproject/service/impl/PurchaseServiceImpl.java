package com.example.auctionproject.service.impl;

import com.example.auctionproject.convertor.PurchaseConvertor;
import com.example.auctionproject.dto.purchase.PurchaseCreateDto;
import com.example.auctionproject.dto.purchase.PurchaseDisplayDto;
import com.example.auctionproject.model.Purchase;
import com.example.auctionproject.repository.PurchaseRepository;
import com.example.auctionproject.service.PurchaseService;
import org.springframework.stereotype.Service;

@Service
public class PurchaseServiceImpl implements PurchaseService {
    private final PurchaseRepository purchaseRepository;
    private final PurchaseConvertor purchaseConvertor;

    public PurchaseServiceImpl(PurchaseRepository purchaseRepository, PurchaseConvertor purchaseConvertor) {
        this.purchaseRepository = purchaseRepository;
        this.purchaseConvertor = purchaseConvertor;
    }

    @Override
    public PurchaseDisplayDto createPurchase(PurchaseCreateDto purchaseCreateDto) {
        Purchase purchase = purchaseRepository.save(purchaseConvertor.createDtoToEntity(purchaseCreateDto));

        return purchaseConvertor.entityToDisplayDto(purchase);
    }
}

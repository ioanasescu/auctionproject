package com.example.auctionproject.service.impl;

import com.example.auctionproject.convertor.UserConvertor;
import com.example.auctionproject.convertor.UserUpdateConvertor;
import com.example.auctionproject.dto.user.UserCreateDto;
import com.example.auctionproject.dto.user.UserDisplayDto;
import com.example.auctionproject.dto.user.UserUpdateDto;
import com.example.auctionproject.exceptions.UserException;
import com.example.auctionproject.model.User;
import com.example.auctionproject.repository.UserRepository;
import com.example.auctionproject.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserConvertor userConvertor;
    private final UserUpdateConvertor userUpdateConvertor;

    public UserServiceImpl(UserRepository userRepository, UserConvertor userConvertor, UserUpdateConvertor userUpdateConvertor) {
        this.userRepository = userRepository;
        this.userConvertor = userConvertor;
        this.userUpdateConvertor = userUpdateConvertor;
    }

    @Override
    public UserDisplayDto createUser(UserCreateDto userCreateDto) {
        User user = userRepository.save(userConvertor.createDtoToEntity(userCreateDto));
        return userConvertor.entityToDisplayDto(user);
    }

    @Override
    public UserDisplayDto findByEmail(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new UserException("No account with email " +
                email + "found"));
        return userConvertor.entityToDisplayDto(user);
    }

    @Override
    public List<UserDisplayDto> findAllUsers() {
        List<UserDisplayDto> userDisplayDtos = new ArrayList<>();
        userRepository.findAll().forEach(user -> userDisplayDtos.add(userConvertor.entityToDisplayDto(user)));
        return userDisplayDtos;
    }

    @Override
    public UserDisplayDto updateUser(UserUpdateDto userUpdateDto) {
        User user = userRepository.save(userUpdateConvertor.updateDtoToEntity(userUpdateDto));
        return userUpdateConvertor.entityToDisplayDto(user);
    }

}

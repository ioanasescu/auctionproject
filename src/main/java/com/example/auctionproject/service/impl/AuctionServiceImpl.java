package com.example.auctionproject.service.impl;

import com.example.auctionproject.convertor.AuctionConvertor;
import com.example.auctionproject.dto.auction.AuctionCreateDto;
import com.example.auctionproject.dto.auction.AuctionDisplayDto;
import com.example.auctionproject.model.Auction;
import com.example.auctionproject.repository.AuctionRepository;
import com.example.auctionproject.service.AuctionService;
import org.springframework.stereotype.Service;

@Service
public class AuctionServiceImpl implements AuctionService {
    private final AuctionRepository auctionRepository;
    private final AuctionConvertor auctionConvertor;

    public AuctionServiceImpl(AuctionRepository auctionRepository, AuctionConvertor auctionConvertor) {
        this.auctionRepository = auctionRepository;
        this.auctionConvertor = auctionConvertor;
    }

    @Override
    public AuctionDisplayDto createAuction(AuctionCreateDto auctionCreateDto) {
        Auction auction =  auctionRepository.save(auctionConvertor.createDtoToEntity(auctionCreateDto));
        return auctionConvertor.entityToDisplayDto(auction);
    }
}

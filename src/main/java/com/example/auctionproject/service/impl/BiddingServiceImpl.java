package com.example.auctionproject.service.impl;

import com.example.auctionproject.convertor.BiddingConvertor;
import com.example.auctionproject.dto.bidding.BiddingCreateDto;
import com.example.auctionproject.dto.bidding.BiddingDisplayDto;
import com.example.auctionproject.model.Bidding;
import com.example.auctionproject.repository.BiddingRepository;
import com.example.auctionproject.service.BiddingService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BiddingServiceImpl implements BiddingService {

    private final BiddingRepository biddingRepository;
    private final BiddingConvertor biddingConvertor;

    public BiddingServiceImpl(BiddingRepository biddingRepository, BiddingConvertor biddingConvertor) {
        this.biddingRepository = biddingRepository;
        this.biddingConvertor = biddingConvertor;
    }

    @Override
    public BiddingDisplayDto createBidding(BiddingCreateDto biddingCreateDto) {
        Bidding bidding = biddingRepository.save(biddingConvertor.createDtoToEntity(biddingCreateDto));
        return biddingConvertor.entityToDisplayDto(bidding);
    }

    @Override
    public List<BiddingDisplayDto> findAllBidding() {
        List<BiddingDisplayDto> biddings = new ArrayList<>();
        biddingRepository.findAll().forEach(bidding -> biddings.add(biddingConvertor.entityToDisplayDto(bidding)));
        return biddings;
    }
}

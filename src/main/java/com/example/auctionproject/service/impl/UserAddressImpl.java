package com.example.auctionproject.service.impl;

import com.example.auctionproject.convertor.UserAddressConvertor;
import com.example.auctionproject.dto.user.UserAddressCreateDto;
import com.example.auctionproject.dto.user.UserAddressDisplayDto;
import com.example.auctionproject.model.UserAddress;
import com.example.auctionproject.repository.UserAddressRepository;
import com.example.auctionproject.service.UserAddressService;
import org.springframework.stereotype.Service;

@Service
public class UserAddressImpl implements UserAddressService {
    private final UserAddressRepository userAddressRepository;

    public UserAddressImpl(UserAddressRepository userAddressRepository) {
        this.userAddressRepository = userAddressRepository;
    }

    @Override
    public UserAddressDisplayDto createUserAddress(UserAddressCreateDto userAddressCreateDto) {
        UserAddress userAddress =  userAddressRepository.save(UserAddressConvertor.createDtoToEntity(userAddressCreateDto));
        return UserAddressConvertor.entityToDisplayDto(userAddress);
    }
}

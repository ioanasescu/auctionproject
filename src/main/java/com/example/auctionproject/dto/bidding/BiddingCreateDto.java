package com.example.auctionproject.dto.bidding;

import com.example.auctionproject.model.Auction;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Negative;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.repository.Query;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BiddingCreateDto {
    @NotNull
    private Long auctionId;
    @NotNull
    private Long userBiddingId;
    @NotNull(message = "Please add the amount needed for your product")
    private Integer amount;
}

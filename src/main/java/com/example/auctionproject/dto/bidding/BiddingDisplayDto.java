package com.example.auctionproject.dto.bidding;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BiddingDisplayDto {
    private Long id;
    private Long auctionId;
    private Long userBiddingId;
    private Integer amount;

}

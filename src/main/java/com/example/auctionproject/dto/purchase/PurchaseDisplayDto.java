package com.example.auctionproject.dto.purchase;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PurchaseDisplayDto {
    private Long id;
    private Long auctionId;
    private Long userBiddingId;
    private Long biddingId;
    private Integer amount;
}

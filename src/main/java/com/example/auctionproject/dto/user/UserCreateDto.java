package com.example.auctionproject.dto.user;

import com.example.auctionproject.model.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateDto {
    @NotBlank(message = "Email is mandatory")
    @Email(message = "Email is not valid")
    private String email;
    @NotBlank(message = "Password is mandatory")
    private String password;
    @NotBlank(message = "Account Name is mandatory")
    private String accountName;
    @NotNull(message = "Your Address is mandatory")
    private UserAddress userAddress;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateOfAccountCreation = LocalDateTime.now();
    private UserAccountStatus accountStatus = UserAccountStatus.INACTIVE;
    private String avatar;
    private UserType userType = UserType.NORMAL;
    private UserRole userRole = UserRole.USER;
}

package com.example.auctionproject.dto.user;

import com.example.auctionproject.model.UserAccountStatus;
import com.example.auctionproject.model.UserAddress;
import com.example.auctionproject.model.UserRole;
import com.example.auctionproject.model.UserType;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateDto {
    @NotNull(message = "ID must not be null")
    private Long id;
    @NotBlank(message = "Email is mandatory")
    @Email(message = "Email is not valid")
    private String email;
    @NotBlank(message = "Password is mandatory")
    private String password;
    @NotBlank(message = "Account Name is mandatory")
    private String accountName;
}

package com.example.auctionproject.dto.user;

import com.example.auctionproject.model.*;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDisplayDto {
    private Long id;
    private String email;
    private String password;
    private String accountName;
    private UserAddress userAddress;
    private LocalDateTime dateOfAccountCreation;
    private UserAccountStatus accountStatus;
    private String avatar;
    private UserType userType;
    private UserRole userRole;


}

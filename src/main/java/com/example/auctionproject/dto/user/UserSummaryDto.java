package com.example.auctionproject.dto.user;

import com.example.auctionproject.model.UserAccountStatus;
import com.example.auctionproject.model.UserAddress;
import com.example.auctionproject.model.UserRole;
import com.example.auctionproject.model.UserType;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserSummaryDto {
    private Long id;
    private String email;
    private String accountName;
    private LocalDateTime dateOfAccountCreation;
    private UserAccountStatus accountStatus;
    private UserType userType;
    private UserRole userRole;
}

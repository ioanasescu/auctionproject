package com.example.auctionproject.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserAddressCreateDto {
    private String province;
    private String city;
    private String street;
    private String houseNumber;
    private String zipCode;
}

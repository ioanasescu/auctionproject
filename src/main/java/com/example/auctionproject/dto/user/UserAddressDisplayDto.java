package com.example.auctionproject.dto.user;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAddressDisplayDto {
    private Long id;
    private String province;
    private String city;
    private String street;
    private String houseNumber;
    private String zipCode;
}

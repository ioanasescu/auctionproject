package com.example.auctionproject.dto.auction;

import com.example.auctionproject.model.AuctionCategory;
import com.example.auctionproject.model.User;
import com.example.auctionproject.model.UserAddress;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuctionDisplayDto {
    private Long id;
    private String title;
    private String description;
    private String photos;
    private AuctionCategory auctionCategory;
    private Double minAmount;
    private boolean promoted;
    private UserAddress userAddress;
    private LocalDateTime dateOfIssue;
    private LocalDateTime endDate;
    private Integer numberOfVisits;
    private Long userId;
}

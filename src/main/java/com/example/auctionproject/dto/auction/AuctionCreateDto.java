package com.example.auctionproject.dto.auction;

import com.example.auctionproject.model.AuctionCategory;
import com.example.auctionproject.model.User;
import com.example.auctionproject.model.UserAddress;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.thymeleaf.util.DateUtils;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuctionCreateDto {
    @NotBlank(message = "Title is mandatory")
    private String title;
    @NotBlank(message = "Description is mandatory")
    private String description;
    private String photos;
    @NotNull(message = "Auction Category is mandatory")
    private AuctionCategory auctionCategory;
    @NotNull(message = "Minim Amount is mandatory")
    private Double minAmount;
    private boolean promoted;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateOfIssue = LocalDateTime.now();
    @NotNull(message = "End date is mandatory")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endDate = dateOfIssue.plusDays(7);
    private Integer numberOfVisits;
    private Long userId;


}

package com.example.auctionproject.convertor;

import com.example.auctionproject.dto.auction.AuctionCreateDto;
import com.example.auctionproject.dto.auction.AuctionDisplayDto;
import com.example.auctionproject.model.Auction;
import com.example.auctionproject.repository.UserAddressRepository;
import com.example.auctionproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuctionConvertor {
    @Autowired
    private final UserAddressRepository userAddressRepository;
    private final UserRepository userRepository;

    public AuctionConvertor(UserAddressRepository userAddressRepository, UserRepository userRepository) {
        this.userAddressRepository = userAddressRepository;
        this.userRepository = userRepository;
    }

    public Auction createDtoToEntity(AuctionCreateDto auctionCreateDto){
        return Auction.builder()
                .title(auctionCreateDto.getTitle())
                .description(auctionCreateDto.getDescription())
                .photos(auctionCreateDto.getPhotos())
                .auctionCategory(auctionCreateDto.getAuctionCategory())
                .minAmount(auctionCreateDto.getMinAmount())
                .promoted(auctionCreateDto.isPromoted())
                .user(userRepository.findById(auctionCreateDto.getUserId()).get())
                .userAddress(userRepository.findById(auctionCreateDto.getUserId()).get().getUserAddress())
                .dateOfIssue(auctionCreateDto.getDateOfIssue())
                .endDate(auctionCreateDto.getEndDate())
                .numberOfVisits(auctionCreateDto.getNumberOfVisits())
                .build();
    }

    public AuctionDisplayDto entityToDisplayDto(Auction auction){
        return AuctionDisplayDto.builder()
                .title(auction.getTitle())
                .description(auction.getDescription())
                .photos(auction.getPhotos())
                .auctionCategory(auction.getAuctionCategory())
                .minAmount(auction.getMinAmount())
                .promoted(auction.isPromoted())
                .userAddress(auction.getUserAddress())
                .dateOfIssue(auction.getDateOfIssue())
                .endDate(auction.getEndDate())
                .numberOfVisits(auction.getNumberOfVisits())
                .userId(auction.getUser().getId())
                .build();
    }
}

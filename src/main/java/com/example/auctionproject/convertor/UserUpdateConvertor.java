package com.example.auctionproject.convertor;

import com.example.auctionproject.dto.user.UserDisplayDto;
import com.example.auctionproject.dto.user.UserUpdateDto;
import com.example.auctionproject.model.User;
import com.example.auctionproject.repository.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class UserUpdateConvertor {
    private final UserRepository userRepository;

    public UserUpdateConvertor(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    public User updateDtoToEntity(UserUpdateDto userUpdateDto){
        return User.builder()
                .id(userUpdateDto.getId())
                .email(userUpdateDto.getEmail())
                .password(userUpdateDto.getPassword())
                .accountName(userUpdateDto.getAccountName())
                .userAddress(userRepository.findById(userUpdateDto.getId()).get().getUserAddress())
                .dateOfAccountCreation(userRepository.findById(userUpdateDto.getId()).get().getDateOfAccountCreation())
                .accountStatus(userRepository.findById(userUpdateDto.getId()).get().getAccountStatus())
                .avatar(userRepository.findById(userUpdateDto.getId()).get().getAvatar())
                .userType(userRepository.findById(userUpdateDto.getId()).get().getUserType())
                .userRole(userRepository.findById(userUpdateDto.getId()).get().getUserRole())
                .build();
    }

    public UserDisplayDto entityToDisplayDto(User user){
        return UserDisplayDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .password(user.getPassword())
                .accountName(user.getAccountName())
                .userAddress(user.getUserAddress())
                .dateOfAccountCreation(user.getDateOfAccountCreation())
                .accountStatus(user.getAccountStatus())
                .avatar(user.getAvatar())
                .userType(user.getUserType())
                .userRole(user.getUserRole())
                .build();
    }
}

package com.example.auctionproject.convertor;

import com.example.auctionproject.dto.user.UserAddressCreateDto;
import com.example.auctionproject.dto.user.UserAddressDisplayDto;
import com.example.auctionproject.model.UserAddress;

public class UserAddressConvertor {
    public static UserAddress createDtoToEntity(UserAddressCreateDto userAddressCreateDto){
        return UserAddress.builder()
                .province(userAddressCreateDto.getProvince())
                .city(userAddressCreateDto.getCity())
                .street(userAddressCreateDto.getStreet())
                .houseNumber(userAddressCreateDto.getHouseNumber())
                .zipCode(userAddressCreateDto.getZipCode())
                .build();
    }

    public static UserAddressDisplayDto entityToDisplayDto(UserAddress userAddress){
        return UserAddressDisplayDto.builder()
                .province(userAddress.getProvince())
                .city(userAddress.getCity())
                .street(userAddress.getStreet())
                .houseNumber(userAddress.getHouseNumber())
                .zipCode(userAddress.getZipCode())
                .build();
    }

}

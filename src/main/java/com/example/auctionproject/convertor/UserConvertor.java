package com.example.auctionproject.convertor;

import com.example.auctionproject.dto.user.UserCreateDto;
import com.example.auctionproject.dto.user.UserDisplayDto;
import com.example.auctionproject.dto.user.UserSummaryDto;
import com.example.auctionproject.dto.user.UserUpdateDto;
import com.example.auctionproject.model.User;
import com.example.auctionproject.repository.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class UserConvertor {

    public User createDtoToEntity(UserCreateDto userCreateDto){

        return User.builder().email(userCreateDto.getEmail())
                .password(userCreateDto.getPassword())
                .accountName(userCreateDto.getAccountName())
                .userAddress(userCreateDto.getUserAddress())
                .dateOfAccountCreation(userCreateDto.getDateOfAccountCreation())
                .accountStatus(userCreateDto.getAccountStatus())
                .avatar(userCreateDto.getAvatar())
                .userType(userCreateDto.getUserType())
                .userRole(userCreateDto.getUserRole())
                .build();
    }

    public UserDisplayDto entityToDisplayDto(User user){
        return UserDisplayDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .password(user.getPassword())
                .accountName(user.getAccountName())
                .userAddress(user.getUserAddress())
                .dateOfAccountCreation(user.getDateOfAccountCreation())
                .accountStatus(user.getAccountStatus())
                .avatar(user.getAvatar())
                .userType(user.getUserType())
                .userRole(user.getUserRole())
                .build();
    }


    public UserSummaryDto entityToSummaryDto(User user){
        return UserSummaryDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .accountName(user.getAccountName())
                .dateOfAccountCreation(user.getDateOfAccountCreation())
                .accountStatus(user.getAccountStatus())
                .userType(user.getUserType())
                .userRole(user.getUserRole())
                .build();
    }
}

package com.example.auctionproject.convertor;

import com.example.auctionproject.dto.purchase.PurchaseCreateDto;
import com.example.auctionproject.dto.purchase.PurchaseDisplayDto;
import com.example.auctionproject.model.Purchase;
import com.example.auctionproject.repository.AuctionRepository;
import com.example.auctionproject.repository.BiddingRepository;
import com.example.auctionproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PurchaseConvertor {

    @Autowired
    private final AuctionRepository auctionRepository;
    private final UserRepository userRepository;

    private final BiddingRepository biddingRepository;

    public PurchaseConvertor(AuctionRepository auctionRepository, UserRepository userRepository, BiddingRepository biddingRepository) {
        this.auctionRepository = auctionRepository;
        this.userRepository = userRepository;
        this.biddingRepository = biddingRepository;
    }

    public Purchase createDtoToEntity(PurchaseCreateDto purchaseCreateDto){
        return Purchase.builder()
                .auction(biddingRepository.findById(purchaseCreateDto.getBiddingId()).get().getAuction())
                .user(biddingRepository.findById(purchaseCreateDto.getBiddingId()).get().getUser())
                .bidding(biddingRepository.findById(purchaseCreateDto.getBiddingId()).get())
                .amount(biddingRepository.findById(purchaseCreateDto.getBiddingId()).get().getAmount())
                .build();
    }

    public PurchaseDisplayDto entityToDisplayDto(Purchase purchase){
        return  PurchaseDisplayDto.builder()
                .auctionId(purchase.getAuction().getId())
                .userBiddingId(purchase.getUser().getId())
                .biddingId(purchase.getBidding().getId())
                .amount(purchase.getAmount())
                .build();
    }
}

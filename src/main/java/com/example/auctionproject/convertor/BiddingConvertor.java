package com.example.auctionproject.convertor;

import com.example.auctionproject.dto.bidding.BiddingCreateDto;
import com.example.auctionproject.dto.bidding.BiddingDisplayDto;
import com.example.auctionproject.model.Bidding;
import com.example.auctionproject.repository.AuctionRepository;
import com.example.auctionproject.repository.BiddingRepository;
import com.example.auctionproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BiddingConvertor {
    @Autowired
    private final AuctionRepository auctionRepository;
    private final UserRepository userRepository;
    private final BiddingRepository biddingRepository;

    public BiddingConvertor(AuctionRepository auctionRepository, UserRepository userRepository, BiddingRepository biddingRepository) {
        this.auctionRepository = auctionRepository;
        this.userRepository = userRepository;
        this.biddingRepository = biddingRepository;
    }

    public Bidding createDtoToEntity(BiddingCreateDto biddingCreateDto){
        return Bidding.builder()
                .auction(auctionRepository.findById(biddingCreateDto.getAuctionId()).get())
                .user(userRepository.findById(biddingCreateDto.getUserBiddingId()).get())
                .amount(biddingCreateDto.getAmount())
                .build();
    }

    public BiddingDisplayDto entityToDisplayDto(Bidding bidding){
        return  BiddingDisplayDto.builder()
                .auctionId(bidding.getAuction().getId())
                .userBiddingId(bidding.getUser().getId())
                .amount(bidding.getAmount())
                .build();
    }
}

package com.example.auctionproject.exceptions;

public class UserException extends RuntimeException{
    public UserException(String message)
    {
        super(message);
    };
}
